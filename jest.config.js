module.exports = {
    transform: {
      '^.+\\.(tsx|ts)?$': 'ts-jest',
      '^.+\\.(js|jsx|mjs)?$': 'babel-jest',
    },
    testEnvironment: 'jsdom',
    coverageReporters: ['text', 'lcov', 'json', 'cobertura'],
    testMatch: ['/*.test.(ts|tsx)'],
    moduleFileExtensions: ['ts', 'js', 'mjs', 'node'],
    collectCoverageFrom: [
      'src//*.{js,jsx,ts,tsx}',
      '!src//*.d.ts',
      '!src//*.stories.{js,jsx,ts,tsx}',
      '!src/test/**/*.{js,jsx,ts,tsx}',
    ],
    preset: 'ts-jest',
    roots: ['<rootDir>'],
    watchPathIgnorePatterns: ['pact/logs/*', 'pact/pacts/*'],
  };