import cors from 'cors';
import dotenv from 'dotenv';
import express, { Application } from 'express';

import userRoutes from './users/userRoutes';

dotenv.config();

const app: Application = express();
const port = process.env.PORT || 3001;

const init = () => {
    app.use(cors());
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    
    app.use(userRoutes);
    
    app.listen(port, () => {
        console.log(`Server is running on port ${port}`);
    });
};

init();