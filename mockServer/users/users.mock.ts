const userMock = [
    {
      id: '1',
      email: 'default@mail.com',
      claims: [],
      role: 'USER',
    },
    {
      id: '2',
      email: '2fa@mail.com',
      claims: [
        {
          id: 1,
          name: '2fa',
          priority: 3,
        },
      ],
      role: 'USER',
    },
    {
      id: '2',
      email: 'tc@mail.com',
      claims: [
        {
          id: 2,
          name: 'terms and conditions',
          priority: 2,
        },
      ],
      role: 'USER',
    },
    {
      id: '3',
      email: '2fa_tc@mail.com',
      claims: [
        {
          id: 1,
          name: '2fa',
          priority: 3,
        },
        {
          id: 2,
          name: 'terms and conditions',
          priority: 2,
        },
      ],
      role: 'USER',
    },
    {
      id: '4',
      email: 'change_pass@mail.com',
      claims: [
        {
          id: 3,
          name: 'password update',
          priority: 1,
        },
      ],
      role: 'USER',
    },
    {
      id: '5',
      email: '2fa_change_pass@mail.com',
      claims: [
        {
          id: 1,
          name: '2fa',
          priority: 3,
        },
        {
          id: 3,
          name: 'password update',
          priority: 1,
        },
      ],
      role: 'USER',
    },
    {
      id: '6',
      email: 'tc_change_password@mail.com',
      claims: [
        {
          id: 2,
          name: 'terms and conditions',
          priority: 2,
        },
        {
          id: 3,
          name: 'password update',
          priority: 1,
        },
      ],
      role: 'USER',
    },
    {
      id: '7',
      email: '2fa_tc_change_password@mail.com',
      claims: [
        {
          id: 1,
          name: '2fa',
          priority: 3,
        },
        {
          id: 2,
          name: 'terms and conditions',
          priority: 2,
        },
        {
          id: 3,
          name: 'password update',
          priority: 1,
        },
      ],
      role: 'USER',
    },
  ];

  export default userMock;