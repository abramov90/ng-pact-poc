import express from 'express';

import { getUsers, getUser } from './userController';

const apiPrefix = process.env.API_PREFIX || '/api/v1';

const router = express.Router();

router.get(`${apiPrefix}/users`, getUsers);
router.get(`${apiPrefix}/users/:id`, getUser);

export default router;