import path from "path";

import { Verifier, LogLevel, VerifierOptions } from "@pact-foundation/pact";
import express from "express";

import userRoutes from "./userRoutes";

const app = express();
app.use(userRoutes);
const server = app.listen(3001);

const LOG_LEVEL = process.env.LOG_LEVEL || 'TRACE';

describe("Pact Verification", () => {
    it("validates the expectations of the consumer", () => {
        const opts: VerifierOptions = {
            logLevel: LOG_LEVEL as LogLevel,
            providerBaseUrl: "http://127.0.0.1:3001",
            provider: "UserService",
            providerVersion: "1.0.0",
            pactUrls: [path.resolve(__dirname, "../../pacts/userconsumer-userservice.json")],
        };
    
        return new Verifier(opts).verifyProvider().then((output) => {
            console.log("Pact Verification Complete!");
            console.log(output);
        }).finally(() => {
            server.close();
        });
    });
});