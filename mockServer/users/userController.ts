import { Request, Response } from 'express';

import USERS from './users.mock';

export const getUsers = async (req: Request, res: Response) => {
  res.json(USERS);
};

export const getUser = async (req: Request, res: Response) => {
  const { id } = req.params;
  const user = USERS.find((user) => user.id === id);
  if (user) {
    res.json(user);
  } else {
    res.status(404).json({ error: 'User not found' });
  }
};