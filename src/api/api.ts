import axios from 'axios';

const API_URL = 'http://127.0.0.1:3001';
const API_PREFIX = '/api/v1';

export const getUsers = async () => {
    const response = await axios.get(`${API_URL}${API_PREFIX}/users`);
    return response.data;
};

export const getUser = async (id: string) => {
    const response = await axios.get(`${API_URL}${API_PREFIX}/users/${id}`);
    return response.data;
};