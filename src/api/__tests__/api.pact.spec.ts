import path from "path";

import { PactV3, MatchersV3, SpecificationVersion } from "@pact-foundation/pact";

import { getUsers } from "../../api/api";

const { like } = MatchersV3;

const provider = new PactV3({
    consumer: "UserConsumer",
    provider: "UserService",
    logLevel: "info",
    dir: path.resolve(process.cwd(), "pacts"),
    spec: SpecificationVersion.SPECIFICATION_VERSION_V2,
    host: "127.0.0.1",
    port: 3001,
});

describe("User API", () => {
    describe("getUsers", () => {
        it("returns a list of users", async () => {
            await provider.addInteraction({
                states: [{ description: "a list of users" }],
                uponReceiving: "a request for users",
                withRequest: {
                    method: "GET",
                    path: "/api/v1/users",
                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    body: like([{                        
                        claims: like([{
                            id: like(1),
                            name: like("2fa"),
                            priority: like(3),
                        }]),
                        id: like("1"),
                        email: like("user@email.com"),
                        role: like("USER"),
                    }]),
                },
            });

            await provider.executeTest(async () => {
                const users = await getUsers();
                
                expect(users).toEqual([
                    {
                        id: "1",
                        email: "user@email.com",
                        claims: [
                            {
                                id: 1,
                                name: "2fa",
                                priority: 3,
                            },
                        ],
                        role: "USER",
                    },
                ]);
            });
        });
    });
});
